class CustomDeploy

  def initialize(mina, machine)
    @mina = mina
    @machine = machine
  end

  def deploy
    @mina.set :domain, machine_ip
    @mina.queue  %[echo "-----> Deploing to #{@machine} (#{machine_ip}).\n"]
    @mina.invoke :deploy
  end

  def setup
    @mina.set :domain, machine_ip
    @mina.invoke :setup
  end

  private

  def deploy_to
    @deploy_to ||= @mina.deploy_to
  end

  def machine_ip
    '192.168.1.30'
    # '25.115.147.109' 
    #@machine_ip ||= `hamachi list | grep #{@machine} | awk '{print $4}'`.gsub("\n", "")
  end
end