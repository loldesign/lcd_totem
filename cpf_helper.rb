class CPFHelper
  def self.format(cpf)
    formatted_cpf = ""
    cpf.split("").reverse.each.with_index do |c, i|
      formatted_cpf << "-" if i == 2
      formatted_cpf << "." if i % 3 == 2 && i >= 5
      formatted_cpf << c
    end
    formatted_cpf.reverse
  end
end