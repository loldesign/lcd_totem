# encoding: UTF-8
require "serialport"
require "timeout"
require_relative "environment"
require_relative "lcdio"

class LCD
  attr_accessor :lcdio

  def initialize(logger=nil)
    @lcdio = LCDIO.new(logger) if WEB_TOTEM_ENV == "production"
  end

  def welcome
    write_first_line("Seja Bem Vindo!")
    write_second_line("Digite seu CPF")
  end

  def wait_for_cpf
    wait_for_numbers "Digite seu CPF:", false, "", false, 600
  end

  def wait_for_password
    wait_for_numbers("Senha(4 numeros)", true)
  end

  def wait_for_slot_number
    slots_size = Slot.all.count
    wait_for_numbers("Selecione Baia:", false, "[1-#{slots_size}]: ", true)
  end

  def release_more_bikes?
    wait_for_numbers "Retirar mais?"
  end

  def close
    @lcd_conn.close if WEB_TOTEM_ENV == "production"
  end

  def invalid_cpf
    write_first_line("CPF Invalido.")
    write_second_line("Tente novamente!")
    sleep 2
  end

  def invalid_password
    write_first_line("Senha Invalida.")
    write_second_line("Tente novamente!")
    sleep 2
  end

  def invalid_slot
    write_first_line("Baia invalida")
    write_second_line("Tente novamente!")
    sleep 2
  end

  def no_available_slots
    write_first_line("Sem baias")
    write_second_line("Tente mais tarde")
    sleep 2 
  end

  def inactive_cyclist
    write_first_line("Taxa de cadastro")
    write_second_line("nao paga")
    sleep 4
  end

  def maximum_bikes_retrieved
    write_first_line("Limite maximo")
    write_second_line("de bike excedido")
    sleep 2
  end

  def blocked_cyclist
    write_first_line("Conta Bloqueada")
    erase_second_line
    sleep 4
  end

  def press_button
    write_first_line("Aperte o botao")
    write_second_line("ao lado da bike")
    sleep 3
    write_first_line("ao lado da bike")
    write_second_line("escolhida e a")
    sleep 3
    write_first_line("escolhida e a")
    write_second_line("puxe p/ liberar")
    sleep 3
  end

  def connection_error
    write_first_line("Sem servidor")
    write_second_line("Tente mais tarde")
    sleep 2
  end

  def authenticating
    write_first_line("Autenticando...")
    erase_second_line
    sleep 0.5
  end

  def finding_cyclist
    write_first_line("Procurando")
    write_second_line("usuario...")
    sleep 0.5
  end

  def station_closed
    write_first_line("Estacao")
    write_second_line("Fechada...")
    sleep 1
  end

  def creditcard_expired
    write_first_line("Cartao de ")
    write_second_line("Credito Expirado")
    sleep 1
  end

  def unknow_error_message
    write_first_line("Erro inesperado ")
    erase_second_line
    sleep 1
  end

  def api_error
    write_first_line("Sem conexao")
    write_second_line("Tente mais tarde")
    sleep 2
  end

  def internet_error
    write_first_line("Sem internet")
    write_second_line("Tente mais tarde")
    sleep 2
  end

  def waiting_rent
    write_first_line("Ultima demanda ")
    write_second_line("de emprestimo")
    sleep 3
    write_first_line("de emprestimo")
    write_second_line("em tratamento.")
    sleep 3
    write_first_line("em tratamento.")
    write_second_line("Aguarde 1 minuto")
    sleep 3
  end

  def ticket_expired
    write_first_line("Sem Passe")
    write_second_line("valido")
    sleep 1
  end

  def display_canceled_message
    write_first_line('Cancelado')
    write_second_line('Aguarde...')
    sleep 1
  end

  protected
    def write_first_line(text)
      write_line 1, text
    end
    
    def write_second_line(text)
      write_line 2, text
    end

    def write_line(line, text)
      text = " " if text.blank?
      WEB_TOTEM_ENV == "production" ? @lcdio.write(line, text) : puts("#{line}: #{text}")
    end

    def erase_lines
      @lcdio.erase 0 if WEB_TOTEM_ENV == "production"
    end

    def erase_first_line
      @lcdio.erase 1 if WEB_TOTEM_ENV == "production"
    end

    def erase_second_line
      @lcdio.erase 2 if WEB_TOTEM_ENV == "production"
    end

    def wait_for_numbers(text, mask_input=false, preppend="", trim_zeros=false, first_char_timeout=30)
      write_first_line(text)
      preppend.empty? ? erase_second_line : write_second_line(preppend)
      numbers = []
      timeout = first_char_timeout
      while ( c = get_char(timeout) ) != "\r"
        return :cancel if c == "\e"
        c =~ /[0-9]/ ? numbers << c : numbers.pop
        masked_input = mask_input ? "*" * numbers.size : numbers.join
        write_second_line preppend + masked_input
        timeout = 30
      end
      numbers = numbers.join
      numbers = numbers.to_i.to_s if trim_zeros
      numbers
    end

    def get_char(timeout=30)
      Timeout::timeout(timeout) do
        begin
          system("stty raw -echo")
          str = STDIN.getc
        ensure
          system("stty -raw echo")
        end
        str.chr
      end
    end
end
