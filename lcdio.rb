# encoding: UTF-8

require 'serialport'
require 'logger'
require_relative 'lcd_logger'

class LCDIO
  def initialize(logger=nil)
    @logger = logger || LCDLogger.new('log/lcdio.log')
    open_connection
    at_exit { close }
  end

  def open_connection(columns=16, lines=2)
    @conn = SerialPort.new(Settings.device)
    current_columns, current_lines = read_configuration.map(&:to_i)
    configure(columns, lines) unless columns == current_columns && lines == current_lines
  rescue
    configure columns, lines
    open_connection columns, lines
  end

  def configure(columns, lines)
    send_data [0, columns, lines]
    @logger.debug "configurando display com #{lines} linhas e #{columns} colunas"
    sleep 8
  end

  def read_configuration
    send_data [7]
    command, columns, lines = read_data
    @logger.debug "configuração: #{columns} colunas, #{lines} linhas"
    return columns, lines
  end

  def display_firmware_version
    send_data [5]
    @logger.debug "mostrando versão do firmware"
    sleep 3
  end

  def on
    send_data [3,1]
    @logger.debug "ligando luz"
  end

  def off
    send_data [3,0]
    @logger.debug "desligango luz"
  end

  def readline(line)
    send_data [6, line]
    command, line, message = read_data
    @logger.debug "linha #{line}: #{message}"
    message
  end

  def write(line, msg)
    msg = msg.to_s[0...16]
    send_data [1, line, msg]
    @logger.debug "escrevendo '#{msg}' na linha #{line}"
    readline line
  end

  def erase(line)
    send_data [2, line]
    @logger.debug "apagando linha #{line}"
    line.zero? ? sleep(3) : readline(line)
  end

  def close
    @logger.debug "fechando lcd"
    @logger.close
    @conn.close
  end

  protected
    def send_data(data)
      @conn.write (data << "\r").join(";")
    end

    def read_data
      data = @conn.readline("\r").split ";"
      data.pop
      data
    end
end
