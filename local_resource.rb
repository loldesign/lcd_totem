require "active_resource"

class LocalResource < ActiveResource::Base
  self.site = Settings.api.path
  self.format = :json
end