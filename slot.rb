require_relative "environment"
require_relative "local_resource"

class Slot < LocalResource

  def self.release(position, user)
    return post("#{position}/release", cpf: user.cpf, role: user.type.downcase)
  rescue
    return false
  end
end
